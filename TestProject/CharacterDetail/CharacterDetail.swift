import Foundation

struct CharacterDetail: Identifiable, Codable {
    let id: Int
    let name: String
    let status: String
    let species: String
    let type: String
    let gender: String
    let origin: Location
    let location: Location
    let image: URL
    let episode: [URL]
    let url: URL
    let created: String

    struct Location: Codable {
        let name: String
        let url: String
    }
}

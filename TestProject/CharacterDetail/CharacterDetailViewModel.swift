import Foundation
import Combine

class CharacterDetailViewModel: ObservableObject {
    @Published var character: CharacterDetail?
    @Published var isLoading = false
    @Published var errorMessage: String?
    let id: Int

    private let repository: CharacterDetailRepository
    private var cancellables = Set<AnyCancellable>()

    init(repository: CharacterDetailRepository, id: Int) {
        self.repository = repository
        self.id = id
    }

    func loadCharacter() {
        isLoading = true
        repository.fetchCharacter(id: id)
            .sink { [weak self] completion in
                self?.isLoading = false
                if case let .failure(error) = completion {
                    self?.errorMessage = error.localizedDescription
                }
            } receiveValue: { [weak self] character in
                self?.character = character
            }
            .store(in: &cancellables)
    }
}

import Combine
import Foundation

class CharacterDetailRepository {
    private let httpClient: RemoteService
    private let baseUrl = "https://rickandmortyapi.com/api/character"

    init(httpClient: RemoteService) {
        self.httpClient = httpClient
    }

    func fetchCharacter(id: Int) -> AnyPublisher<CharacterDetail, Error> {
        guard let url = URL(string: "\(baseUrl)/\(id)") else {
            fatalError("Invalid URL")
        }
        return httpClient.performRequest(url: url, method: "GET")
    }
}

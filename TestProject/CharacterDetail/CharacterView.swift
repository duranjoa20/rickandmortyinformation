import SwiftUI
import Combine

struct CharacterView: View {
    let character: CharacterDetail

    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 10) {
                AsyncImage(url: character.image) { image in
                    image.resizable()
                } placeholder: {
                    ProgressView()
                }
                .aspectRatio(contentMode: .fit)
                .frame(maxWidth: .infinity)

                Text("Name: \(character.name)")
                    .font(.headline)
                Text("Status: \(character.status)")
                Text("Species: \(character.species)")
                Text("Type: \(character.type.isEmpty ? "N/A" : character.type)")
                Text("Gender: \(character.gender)")
                Text("Origin: \(character.origin.name)")
                Text("Location: \(character.location.name)")
                Text("Number of Episodes: \(character.episode.count)")

                // Additional details...
            }
            .padding()
        }
    }
}

struct CharacterView_Previews: PreviewProvider {
    static var previews: some View {
        // Create a sample character for the preview
        let sampleCharacter = CharacterDetail(
            id: 2, name: "Morty Smith", status: "Alive", species: "Human",
            type: "", gender: "Male",
            origin: CharacterDetail.Location(name: "unknown", url: ""),
            location: CharacterDetail.Location(name: "Citadel of Ricks", url: "https://rickandmortyapi.com/api/location/3"),
            image: URL(string: "https://rickandmortyapi.com/api/character/avatar/2.jpeg")!,
            episode: [URL(string: "https://rickandmortyapi.com/api/episode/1")!],
            url: URL(string: "https://rickandmortyapi.com/api/character/2")!,
            created: "2017-11-04T18:50:21.651Z"
        )

        CharacterView(character: sampleCharacter)
    }
}

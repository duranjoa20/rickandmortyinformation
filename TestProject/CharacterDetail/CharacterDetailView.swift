import SwiftUI

struct CharacterDetailView: View {
    @ObservedObject var viewModel: CharacterDetailViewModel

    init(viewModel: CharacterDetailViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        Group {
            if let character = viewModel.character {
                CharacterView(character: character)
            } else if viewModel.isLoading {
                ProgressView()
            } else {
                Text("Unable to load character")
            }
        }
        .onAppear {
            viewModel.loadCharacter()
        }
    }
}

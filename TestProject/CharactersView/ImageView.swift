import SwiftUI
import Combine

struct ImageView: View {
    @StateObject private var viewModel = ImageViewModel()

    let url: String

    var body: some View {
        Group {
            if let image = viewModel.image {
                Image(uiImage: image)
                    .resizable()
            } else {
                ProgressView() // Shows a loading indicator while the image is loading
            }
        }
        .onAppear {
            viewModel.loadImage(from: url)
        }
    }
}

class ImageViewModel: ObservableObject {
    @Published var image: UIImage?
    private var cancellables = Set<AnyCancellable>()

    func loadImage(from urlString: String) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data) }
            .replaceError(with: nil)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in self?.image = $0 }
            .store(in: &cancellables)
    }
}

import Combine

class CharactersViewModel: ObservableObject {
    @Published var characters: [RMCharacter] = []
    @Published var errorMessage: String?
    private var cancellables: Set<AnyCancellable> = []
    private let repository = RickAndMortyRepository(remoteService: RemoteService())
    private var currentPage = 1

    func loadCharacters() {
        repository
            .fetchCharacters(page: currentPage)
            .sink(receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    self.errorMessage = error.localizedDescription
                }
            }, receiveValue: { characters in
                print("calling on appear with page number: \(self.currentPage)")
                self.currentPage += 1
                self.characters.append(contentsOf: characters)
            })
            .store(in: &cancellables)
    }
}

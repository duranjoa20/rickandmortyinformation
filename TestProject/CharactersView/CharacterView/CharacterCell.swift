import SwiftUI

struct CharacterCell: View, Hashable {
    var character: RMCharacter

    var body: some View {
        VStack {
            ImageView(url: character.image)
//                .resizable()
                .aspectRatio(contentMode: .fit)
                .clipShape(RoundedRectangle(cornerRadius: 25, style: .continuous))
            Text(character.name)
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(.headline)
                
            Text(character.status)
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(.subheadline)
                .foregroundStyle(.gray)

        }
        .padding()
        .shadow(color: .gray, radius: 10, x: 0, y: 4)
    }
}


#Preview {
    CharacterCell(character: getCharacterView(amount: 1).first!)
}

//
//  CharacterView.swift
//  TestProject
//
//  Created by Joaquin Duran on 22/01/2024.
//

import Foundation

struct RMCharacter: Identifiable, Hashable {
    let id: Int
    let name: String
    let status: String
    let image: String // This should be the name of the image in your assets folder
}

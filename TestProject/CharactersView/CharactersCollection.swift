import SwiftUI


//struct CharactersCollection: View {
//    // Assume you have an array of characters to display
//    @StateObject var viewModel = CharactersViewModel()
//
//    let columns: [GridItem] = Array(repeating: .init(.flexible()), count: 2)
//
//    var body: some View {
//        NavigationView {
//            ScrollView {
//                LazyVGrid(columns: columns, spacing: 20) {
//                    ForEach(viewModel.characters, id: \.self) { character in
//                        CharacterCell(character: character)
//                    }
//                }
//            }
//            .navigationTitle("Characters")
//            .onAppear {
//                     viewModel.loadCharacters()
//                 }
//            .alert(item: $viewModel.errorMessage) { errorMessage in
//                Alert(title: Text("Error"), message: Text(errorMessage), dismissButton: .default(Text("OK")))
//            }
//            
//        }
//    }
//}
struct CharactersView: View {
    @StateObject var viewModel = CharactersViewModel()
        let columns: [GridItem] = Array(repeating: .init(.flexible()), count: 2)

    var body: some View {
        NavigationView {
            ScrollView {
                LazyVGrid(columns: columns, spacing: 20) {
                    ForEach(viewModel.characters, id: \.self) { character in
                        if isLastElement(colecction: viewModel.characters, id: character.id) {
                            CharacterCell(character: character)
                                .onAppear {
                                    viewModel.loadCharacters()
                            }
                        } else {
                            NavigationLink(destination: RMFactory().makeCharacterDetailView(id: character.id)) {
                                CharacterCell(character: character)
                            }
                            
                        }
                        }
                    }
                }
            .navigationTitle("Rick and Morty Characters")
            .onAppear {
                viewModel.loadCharacters()
            }
        }
    }
}

struct CharactersView_Previews: PreviewProvider {
    static var previews: some View {
        CharactersView()
    }
}

func isLastElement(colecction: [RMCharacter], id: Int) -> Bool {
    return colecction.last?.id == id
    
}

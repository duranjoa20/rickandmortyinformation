import Foundation
import Combine

class RickAndMortyRepository {
    private let remoteService: RemoteService
    private let baseUrl = "https://rickandmortyapi.com/api"
    
    init(remoteService: RemoteService) {
        self.remoteService = remoteService
    }
    
    func fetchCharacters(page: Int) -> AnyPublisher<[RMCharacter], Error> {
        let characterURL = URL(string: "\(baseUrl)/character?page=\(page)")!
        let result: AnyPublisher<CharacterDTO, Error> = remoteService.performRequest(page: page, url: characterURL, method: "GET")
        
        return result
            .map { characterDTO in
                    characterDTO.results.map { characterDto in
                        RMCharacter(id: characterDto.id, name: characterDto.name, status: characterDto.status, image: characterDto.image)
                }
            }.mapError { error in
                error
            }.eraseToAnyPublisher()
    }
}

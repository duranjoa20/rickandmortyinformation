import Foundation

// MARK: - CharacterDTO
struct CharacterDTO: Codable {
    let info: Info
    let results: [Character]
}

// MARK: - Character
struct Character: Codable {
    let id: Int
    let name, status, species, type: String
    let gender: String
    let origin, location: LocationReference
    let image: String
    let episode: [String]
    let url: String
    let created: String
}

// MARK: - LocationReference
struct LocationReference: Codable {
    let name: String
    let url: String
}

// MARK: - Info
struct Info: Codable {
    let count, pages: Int
    let next: String?
    let prev: String?
}

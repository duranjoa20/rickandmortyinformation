import Foundation


class RMFactory: Factory {
    func makeRemoteService() -> RemoteService { RemoteService() }
    
    func makeCharacterDetailRepository(remoteService: RemoteService) -> CharacterDetailRepository {
        let remoteService = makeRemoteService()
        return CharacterDetailRepository(httpClient: remoteService)
    }
    
    func makeCharactersViewModel(remoteService: RemoteService) -> CharactersViewModel {
        return CharactersViewModel()
    }
    
    func makeCharacterDetailViewModel(id: Int) -> CharacterDetailViewModel {
        let remoteClient = makeRemoteService()
        let characterDetailRepository = CharacterDetailRepository(httpClient: remoteClient)
        return CharacterDetailViewModel(repository: characterDetailRepository, id: id)
    }
    
    func makeCharacterDetailView(id: Int) -> CharacterDetailView {
        let viewModel = makeCharacterDetailViewModel(id: id)
        return CharacterDetailView(viewModel: viewModel)
    }
}


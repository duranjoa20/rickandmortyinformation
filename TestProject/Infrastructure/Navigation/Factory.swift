
protocol Factory {
    func makeRemoteService() -> RemoteService
    func makeCharacterDetailRepository(remoteService: RemoteService) -> CharacterDetailRepository
    func makeCharactersViewModel(remoteService: RemoteService) -> CharactersViewModel
    func makeCharacterDetailViewModel(id: Int) -> CharacterDetailViewModel
    func makeCharacterDetailView(id: Int) -> CharacterDetailView
}
